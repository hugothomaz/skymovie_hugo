package com.hugothomaz.empresashugo.data.network


import android.content.Context
import android.support.annotation.NonNull
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie
import java.util.concurrent.TimeUnit

class WebServiceMovies(val context: Context) {

    private val client: OkHttpClient  by lazy{
        OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    private val gson : Gson  by lazy {
        GsonBuilder()
            .create()
    }

    private val adapterRXJava : RxJava2CallAdapterFactory by lazy {
        RxJava2CallAdapterFactory.create()
    }

    private val converterFactory: GsonConverterFactory by lazy {
        GsonConverterFactory.create(gson)
    }

    private val retrofit : Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(ConstantsService.BASE_URL)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(adapterRXJava)
            .client(client)
            .build()
    }

    private val moviesAPI : MoviesAPI by lazy {
        retrofit.create(MoviesAPI::class.java)
    }




    fun getMoviews() : Observable<ArrayList<Movie>> {
        return moviesAPI.getListMovies()
    }


}