package sky.com.hugothomaz.skymobileexercice.util

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.widget.ImageView
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import android.view.View
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions



class UtilDownloadImage {

    companion object {
        @SuppressLint("CheckResult")
        fun downloadImage(url: String?, imageView: View, context : Context) {
            var handler = Handler()

            Thread() {
                run {

                    var requestOptions = RequestOptions()
                    try {

                        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
                        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        requestOptions.priority(Priority.HIGH)

                        handler.post {
                            Glide
                                .with(context.applicationContext)
                                .load(url)
                                .apply(requestOptions)
                                .transition(GenericTransitionOptions.with(android.R.anim.fade_in))
                                .into(imageView as ImageView)
                        }



                    } catch (e: Exception) {
                        throw Exception("Não foi possivel realizar o download da imagem", e)
                    }


                }
            }.start()
        }








    }


}