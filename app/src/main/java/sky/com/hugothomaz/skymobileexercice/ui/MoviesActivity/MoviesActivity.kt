package sky.com.hugothomaz.skymobileexercice.ui.MoviesActivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_movies.*
import sky.com.hugothomaz.skymobileexercice.R
import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie
import sky.com.hugothomaz.skymobileexercice.ui.DetailMovie.DetailMovieIntent


private val TAG = "MoviesActivity"

class MoviesActivity : AppCompatActivity() , MoviesView, MoviewsRecyclerAdapter.ClickMovieListener {

    private lateinit var gridLayoutManager : GridLayoutManager
    private lateinit var movieRecyclerAdapter : MoviewsRecyclerAdapter

    private var broadcastReceiveConnection = BroadcastConnectionCheck()

    private var isMessageContainerVisible = false

    private val presenterMovies : IMoviesPresenter by lazy {
        MoviesPresenter(this, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        initComponents()
    }



    private fun initComponents(){
        var intetntFilter = IntentFilter()
        intetntFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        intetntFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED")
        registerReceiver(broadcastReceiveConnection, intetntFilter)

        toolbarMovies.setTitle(resources.getString(R.string.app_name))
        setSupportActionBar(toolbarMovies)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        configRecycler()

        getMovies()
    }

    private fun configRecycler(){
        gridLayoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)

        movieRecyclerAdapter = MoviewsRecyclerAdapter(this)

        recyclerViewMovies.setHasFixedSize(true)
        recyclerViewMovies.layoutManager = gridLayoutManager
        recyclerViewMovies.adapter = movieRecyclerAdapter

        movieRecyclerAdapter.addClickMovieListener(this)

    }


    override fun setListMovies(listMovies: ArrayList<Movie>) {
        movieRecyclerAdapter.addListMovie(listMovies)
        animationHideLoad()

        if(isMessageContainerVisible){
            animationHideMessageContainer()
        }
    }

    override fun showLoad() {
        animationShowLoad()
    }


    override fun showMessage(message: String) {
        Snackbar.make(rootMovies, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showContainerMessage(imageResource: Int, message: String) {
        imageCotainerMessage.setImageResource(imageResource)
        textViewContainerMessage.setText(message)
        animationShowMessageContainer()
    }

    private fun animationShowLoad(){
        progressMovies.alpha = 0f
        progressMovies.visibility = View.VISIBLE
        val animationShowProgressBar = progressMovies.animate().alpha(1f).setDuration(300L)

        containerMovies.alpha = 1f
        containerMovies.visibility = View.VISIBLE
        val animationShowRootMovies = containerMovies.animate().alpha(0f).setDuration(250L)

        animationShowProgressBar.start()
        animationShowRootMovies.start()

    }


    private fun animationHideLoad(){
        progressMovies.alpha = 1f
        progressMovies.visibility = View.VISIBLE
        val animationShowProgressBar = progressMovies.animate().alpha(0f).setDuration(300L)

        containerMovies.alpha = 0f
        containerMovies.visibility = View.VISIBLE
        val animationShowRootMovies = containerMovies.animate().alpha(1f).setDuration(250L)

        animationShowProgressBar.start()
        animationShowRootMovies.start()
    }


    private fun animationShowMessageContainer(){
        messageContainer.alpha = 0f
        messageContainer.visibility = View.VISIBLE
        val animationShowContainer = messageContainer.animate().alpha(1f).setDuration(300L)
        animationShowContainer.start()
        isMessageContainerVisible = true

        progressMovies.alpha = 1f
        progressMovies.visibility = View.VISIBLE
        val animationShowProgressBar = progressMovies.animate().alpha(0f).setDuration(300L)
        animationShowProgressBar.start()

        containerMovies.alpha = 1f
        containerMovies.visibility = View.VISIBLE
        val animationShowRootMovies = containerMovies.animate().alpha(0f).setDuration(250L)
        animationShowRootMovies.start()
    }


    private fun animationHideMessageContainer(){
        messageContainer.alpha = 1f
        messageContainer.visibility = View.VISIBLE
        val animationShowContainer = messageContainer.animate().alpha(0f).setDuration(300L)
        animationShowContainer.start()

        isMessageContainerVisible = false

        containerMovies.alpha = 0f
        containerMovies.visibility = View.VISIBLE
        val animationShowRootMovies = containerMovies.animate().alpha(1f).setDuration(250L)
        animationShowRootMovies.start()


    }

    private fun getMovies(){
        animationShowLoad()
        presenterMovies.getMovies()
    }

    override fun onClick(movie: Movie) {
        val intent = DetailMovieIntent()
        intent.putExtra(Movie.MOVIE, movie)
        startActivity(intent)
    }

    override fun onRestart() {
        super.onRestart()
        getMovies()
    }


    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiveConnection)
    }


    inner class BroadcastConnectionCheck : BroadcastReceiver() {


        override fun onReceive(context: Context?, intent: Intent?) {
            presenterMovies.checkConnection()
        }

    }


}
