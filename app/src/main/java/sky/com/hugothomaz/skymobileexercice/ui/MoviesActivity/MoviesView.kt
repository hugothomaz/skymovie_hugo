package sky.com.hugothomaz.skymobileexercice.ui.MoviesActivity

import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie

interface MoviesView {

    fun setListMovies(listMovies : ArrayList<Movie>)

    fun showLoad()
    fun showMessage(message : String)

    fun showContainerMessage(imageResource : Int, message : String)

}