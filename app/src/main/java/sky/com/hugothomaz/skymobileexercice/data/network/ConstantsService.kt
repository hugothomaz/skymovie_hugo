package com.hugothomaz.empresashugo.data.network


object ConstantsService {

    const val CONTENT_TYPE_APPLICATION_JSON = "Content-Type: application/json"

    const val BASE_URL = "https://sky-exercise.herokuapp.com/api/"
    const val MOVIES_ENDPOINT = "Movies"

}