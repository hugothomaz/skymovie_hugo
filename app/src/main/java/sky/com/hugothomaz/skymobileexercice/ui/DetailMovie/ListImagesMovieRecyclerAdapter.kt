package sky.com.hugothomaz.skymobileexercice.ui.DetailMovie

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import sky.com.hugothomaz.skymobileexercice.R
import sky.com.hugothomaz.skymobileexercice.util.UtilDownloadImage

class ListImagesMovieRecyclerAdapter : RecyclerView.Adapter<ListImagesMovieRecyclerAdapter.ListImagesMoviewViewHolder> {

    private var listUrlImageMovie: ArrayList<String> = ArrayList()
    private var context: Context

    constructor(context: Context) {
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListImagesMoviewViewHolder {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_recycler_images_movie, parent, false)

        return ListImagesMoviewViewHolder(view)
    }

    override fun getItemCount(): Int {
        listUrlImageMovie?.let {
            return it.size
        }
        return 0
    }

    override fun onBindViewHolder(holderListImages: ListImagesMoviewViewHolder, position: Int) {
        holderListImages.bindMovie(listUrlImageMovie.get(position))
    }

    fun addListUrlImageMoview(listUrlImageMovie: ArrayList<String>) {
        if (listUrlImageMovie.size > 0) {
            this.listUrlImageMovie.clear()
            this.listUrlImageMovie.addAll(listUrlImageMovie)
            notifyDataSetChanged()
        }
    }

    inner class ListImagesMoviewViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val imageViewMoview = view.findViewById<AppCompatImageView>(R.id.imageViewItemListImagesMovie)

        fun bindMovie(urlImage: String) {
            UtilDownloadImage.downloadImage(urlImage, imageViewMoview, context)
        }
    }


}

