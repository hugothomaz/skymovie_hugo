package sky.com.hugothomaz.skymobileexercice.data.network.common.response

import android.os.Parcel
import android.os.Parcelable

class Movie(
    val title: String,
    val overview: String,
    val duration: String,
    val release_year: String,
    val cover_url: String,
    val backdrops_url: ArrayList<String>,
    val id: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.createStringArrayList(),
        source.readString()
    )



    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeString(overview)
        writeString(duration)
        writeString(release_year)
        writeString(cover_url)
        writeStringList(backdrops_url)
        writeString(id)
    }

    companion object {
        val MOVIE = "movie"

        @JvmField
        val CREATOR: Parcelable.Creator<Movie> = object : Parcelable.Creator<Movie> {
            override fun createFromParcel(source: Parcel): Movie = Movie(source)
            override fun newArray(size: Int): Array<Movie?> = arrayOfNulls(size)
        }
    }
}