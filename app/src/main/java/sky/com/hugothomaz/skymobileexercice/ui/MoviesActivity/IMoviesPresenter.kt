package sky.com.hugothomaz.skymobileexercice.ui.MoviesActivity

import io.reactivex.Observable
import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie

interface IMoviesPresenter {

    fun getMovies()

    fun removeObservable()

    fun checkConnection()

}