package sky.com.hugothomaz.skymobileexercice.ui.MoviesActivity

import android.content.Context
import android.support.v4.view.LayoutInflaterCompat
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import sky.com.hugothomaz.skymobileexercice.R
import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie
import sky.com.hugothomaz.skymobileexercice.util.UtilDownloadImage

class MoviewsRecyclerAdapter : RecyclerView.Adapter<MoviewsRecyclerAdapter.MoviewViewHolder> {


    private var listMovies: ArrayList<Movie> = ArrayList()
    private var context: Context
    private var clickMovieListener: ClickMovieListener? = null


    constructor(context: Context) {
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviewViewHolder {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_recycler_movies, parent, false)

        return MoviewViewHolder(view)
    }

    override fun getItemCount(): Int {
        listMovies?.let {
            return it.size
        }

        return 0
    }

    override fun onBindViewHolder(holder: MoviewViewHolder, position: Int) {
        holder.bindMovie(listMovies.get(position))
    }


    fun addClickMovieListener(clickMovieListener: ClickMovieListener) {
        this.clickMovieListener = clickMovieListener
    }

    fun addListMovie(listMovies: ArrayList<Movie>) {
        if (listMovies.size > 0) {
            this.listMovies.clear()
            this.listMovies.addAll(listMovies)
            notifyDataSetChanged()
        }

    }


    inner class MoviewViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private val imageViewMoview = view.findViewById<AppCompatImageView>(R.id.imageViewCover)
        private val title = view.findViewById<TextView>(R.id.textViewTitle)

        fun bindMovie(movie: Movie) {

            view.setOnClickListener {
                clickMovieListener?.let {
                    it.onClick(movie)
                }
            }

            UtilDownloadImage.downloadImage(movie.cover_url, imageViewMoview, context)
            title.setText(movie.title)

        }
    }

    interface ClickMovieListener {
        fun onClick(movie: Movie)
    }

}

