package sky.com.hugothomaz.skymobileexercice.ui.MoviesActivity

import android.content.Context
import com.hugothomaz.empresashugo.data.network.WebServiceMovies
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import sky.com.hugothomaz.skymobileexercice.R
import sky.com.hugothomaz.skymobileexercice.util.VerifyConnectionUtil

private val TAG = "MoviesPresenter"

class MoviesPresenter(val view : MoviesView, val context : Context) : IMoviesPresenter {

    private val webServiceMovies : WebServiceMovies by lazy{
        WebServiceMovies(context)
    }

    lateinit var mCompositeDisposable: Disposable



    override fun getMovies() {

        if(VerifyConnectionUtil.verifyConnectionInternet(context)){
            val observable = webServiceMovies.getMoviews()

            mCompositeDisposable = observable.subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.showLoad()
                }
                .subscribe ({ listMovies ->

                    view.setListMovies(listMovies)
                }, {

                    view.showMessage( "Ocorreu um problema!")

                })
        }else{
            view.showMessage( "Ops, você não esta conectado!")
            view.showContainerMessage(R.drawable.connection_off, context.resources.getString(R.string.message_connection_off))
        }

    }


    override fun removeObservable() {
        mCompositeDisposable.dispose()
    }


    override fun checkConnection() {
        getMovies()
    }




}