package com.hugothomaz.empresashugo.data.network

import io.reactivex.Observable
import retrofit2.http.*
import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie


interface MoviesAPI {

    @GET(ConstantsService.MOVIES_ENDPOINT)
    fun getListMovies(): Observable<ArrayList<Movie>>


}