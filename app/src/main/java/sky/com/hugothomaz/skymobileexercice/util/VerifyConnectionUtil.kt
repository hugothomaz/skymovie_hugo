package sky.com.hugothomaz.skymobileexercice.util

import android.content.Context
import android.net.ConnectivityManager



open class VerifyConnectionUtil {


    companion object {
        fun verifyConnectionInternet(context: Context) : Boolean{

            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            val wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

            return if (!mobile.isConnected && !wifi.isConnected) {
                return false
            } else {
                return true
            }
        }
    }


}