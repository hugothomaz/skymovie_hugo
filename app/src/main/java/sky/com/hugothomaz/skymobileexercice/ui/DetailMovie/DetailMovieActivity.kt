package sky.com.hugothomaz.skymobileexercice.ui.DetailMovie

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_detail_movie.*
import kotlinx.android.synthetic.main.activity_movies.*
import sky.com.hugothomaz.skymobileexercice.R
import sky.com.hugothomaz.skymobileexercice.data.network.common.response.Movie
import sky.com.hugothomaz.skymobileexercice.util.UtilDownloadImage


fun Context.DetailMovieIntent(): Intent {
    return Intent(this, DetailMovieActivity::class.java)
}

class DetailMovieActivity : AppCompatActivity() {

    private lateinit var movie : Movie

    private lateinit var linearLayoutManager : LinearLayoutManager
    private lateinit var listImagesMovieRecyclerAdapter : ListImagesMovieRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        initComponente()
    }


    private fun initComponente(){



        configRecycler()

        intent.getParcelableExtra<Movie>(Movie.MOVIE)?.let{
            movie = it

            UtilDownloadImage.downloadImage(movie.cover_url, imageViewCoverDetailMovie, this)
            textViewOverviewDetailMovie.setText(movie.overview)
            textViewReleaseYearDetailMovie.setText(movie.release_year)
            textViewDurationDetailMovie.setText(movie.duration)
            listImagesMovieRecyclerAdapter.addListUrlImageMoview(movie.backdrops_url)

            toolbarDetailMovie.setTitle(movie.title)
            setSupportActionBar(toolbarDetailMovie)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        }?:animationShowMessageContainer()
    }

    private fun configRecycler(){
        linearLayoutManager = LinearLayoutManager(this,  GridLayoutManager.HORIZONTAL, false)

        listImagesMovieRecyclerAdapter = ListImagesMovieRecyclerAdapter(this)

        recyclerviewImagesDetailMovie.setHasFixedSize(true)
        recyclerviewImagesDetailMovie.layoutManager = linearLayoutManager
        recyclerviewImagesDetailMovie.adapter = listImagesMovieRecyclerAdapter
    }


    private fun animationShowMessageContainer(){
        messageContainerDetailMovie.alpha = 0f
        messageContainerDetailMovie.visibility = View.VISIBLE
        val animationShowContainer = messageContainerDetailMovie.animate().alpha(1f).setDuration(300L)
        animationShowContainer.start()

        progressMovies.alpha = 1f
        progressMovies.visibility = View.VISIBLE
        val animationShowProgressBar = progressMovies.animate().alpha(0f).setDuration(300L)
        animationShowProgressBar.start()

        containerMovies.alpha = 1f
        containerMovies.visibility = View.VISIBLE
        val animationShowRootMovies = containerMovies.animate().alpha(0f).setDuration(250L)
        animationShowRootMovies.start()
    }





}
